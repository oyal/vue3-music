### 后端

进入目录

```Bash
cd server

```

安装依赖

```Bash
npm install

```

运行

```Bash
npm start
```

### 前端

进入目录

```Bash
cd client

```

安装依赖

```Bash
npm install
```

运行

```Bash
npm run dev
```

构建

```Bash
npm run build
```

预览

```Bash
npm run preview
```

