const Koa = require('koa')
const cors = require('@koa/cors')
const bodyParser = require('koa-bodyparser')
const router = require('koa-router')()
const Playlist = require('./module/Playlist')
const LatestMusic = require('./module/LatestMusic')
const LatestMv = require('./module/LatestMv')
const Mv = require('./module/Mv')

require('./config/db')

const app = new Koa()

app.use(cors())
app.use(bodyParser())

router.post('/playlist', async ctx => {
  const isExit = await Playlist.findOne({ id: ctx.request.body.id })
  if (isExit) return (ctx.body = { code: 500, msg: '歌单已存在' })
  ctx.body = await Playlist.create(ctx.request.body)
})

router.post('/latestMusic', ctx => {
  const data = ctx.request.body.data
  data.forEach(async item => {
    const isExit = await LatestMusic.findOne({ id: item.id })
    if (isExit) return (ctx.body = { code: 500, msg: '歌曲已存在' })
    await LatestMusic.create({
      id: item.id,
      name: item.name,
      singers: item.artists,
      album: item.album.name,
      albumId: item.album.id,
      duration: item.duration,
      image: item.album.picUrl,
      mvId: item.mvid
    })
  })
  ctx.body = { code: 200, msg: '歌曲添加成功' }
})

router.post('/latestMv', ctx => {
  const data = ctx.request.body.data
  data.forEach(async item => {
    const isExit = await LatestMv.findOne({ id: item.id })
    if (isExit) return (ctx.body = { code: 500, msg: 'mv已存在' })
    await LatestMv.create({
      id: item.id,
      name: item.name,
      singers: item.artists,
      duration: item.duration,
      image: item.cover,
      playCount: item.playCount
    })
  })
  ctx.body = { code: 200, msg: 'mv添加成功' }
})

router.post('/mv', async ctx => {
  const isExit = await Mv.findOne({ id: ctx.request.body.id })
  if (isExit) return (ctx.body = { code: 500, msg: 'MV已存在' })
  ctx.body = await Mv.create(ctx.request.body)
})

app.use(router.routes()).use(router.allowedMethods())

app.listen(3001, () => {
  console.log('db server running @ http://localhost:3001')
})
