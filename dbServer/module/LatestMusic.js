const mongoose = require('mongoose')

/*
 *
 *  */

const latestMusicSchema = new mongoose.Schema({
  id: {
    // 歌曲id
    type: Number,
    required: true,
    unique: true
  },
  name: {
    // 歌曲名
    type: String
  },
  singers: {
    // 歌手
    type: Array
  },
  album: {
    // 专辑
    type: String
  },
  albumId: {
    // 专辑id
    type: Number
  },
  duration: {
    // 歌曲时长
    type: Number
  },
  image: {
    // 歌曲封面
    type: String
  },
  mvId: {
    // mv id
    type: Number
  }
})

module.exports = mongoose.model('LatestMusic', latestMusicSchema)
