const mongoose = require('mongoose')

const latestMvSchema = new mongoose.Schema({
  id: {
    // mv id
    type: Number,
    required: true,
    unique: true
  },
  name: {
    // mv名
    type: String
  },
  singers: {
    // 歌手
    type: Array
  },
  duration: {
    // mv时长
    type: Number
  },
  image: {
    // mv封面
    type: String
  },
  playCount: {
    // mv播放量
    type: Number
  }
})

module.exports = mongoose.model('LatestMv', latestMvSchema)
