const mongoose = require('mongoose')

const PlaylistSchema = new mongoose.Schema({
  id: {
    // 歌单id
    type: Number,
    required: true,
    unique: true
  },
  name: {
    // 歌单名
    type: String
  },
  description: {
    // 歌单描述
    type: String
  },
  coverImgUrl: {
    // 歌单封面
    type: String
  },
  creator: {
    // 歌单创建者id
    type: Number
  },
  tags: {
    // 歌单标签
    type: Array
  },
  songs: {
    // 歌单歌曲
    type: Array
  },
  playCount: {
    // 歌单播放量
    type: Number
  },
  subscribedCount: {
    // 歌单收藏量
    type: Number
  },
  commentCount: {
    // 歌单评论数
    type: Number
  },
  shareCount: {
    // 歌单分享数
    type: Number
  },
  createTime: {
    // 歌单创建时间
    type: Number
  }
})

module.exports = mongoose.model('Playlist', PlaylistSchema)
