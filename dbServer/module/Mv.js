const mongoose = require('mongoose')

const MvSchema = new mongoose.Schema({
  id: {
    // mv id
    type: Number,
    required: true,
    unique: true
  },
  name: {
    // mv名
    type: String
  },
  singers: {
    // 歌手
    type: Array
  },
  duration: {
    // mv时长
    type: Number
  },
  image: {
    // mv封面
    type: String
  },
  playCount: {
    // mv播放量
    type: Number
  },
  publishTime: {
    // mv发布时间
    type: String
  },
  shareCount: {
    // mv分享数
    type: Number
  },
  subCount: {
    // mv收藏数
    type: Number
  },
  commentCount: {
    // mv评论数
    type: Number
  }
})

module.exports = mongoose.model('Mv', MvSchema)
