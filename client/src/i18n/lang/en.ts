export default {
  projectName: 'Cloud Music Online',
  menus: {
    notLogin: 'Not Login',
    discovery: 'Discovery Music',
    recommend: 'Recommend Playlist',
    latestMusic: 'Latest Music',
    latestMV: 'Latest MV',
    recommendSongs: 'Recommend Songs',
    createPlaylist: 'My Create Playlist',
    collectPlaylist: 'My Collect Playlist'
  },
  message: {
    changeLang: 'Change Language Success',
    changeTheme: 'Change Theme Success'
  },
  latestMusic: {
    all: 'All',
    chinese: 'Chinese',
    western: 'Western',
    japan: 'Japan',
    korea: 'Korea',
    other: 'Other'
  },
  latestMV: {
    area: {
      all: 'All',
      hinterland: 'Hinterland',
      hongkongAndTaiwan: 'Hongkong and Taiwan',
      europeAndAmerica: 'Europe and America',
      japanese: 'Japanese',
      korea: 'Korea',
      other: 'Other'
    },
    type: {
      all: 'All',
      official: 'Official',
      original: 'Original',
      liveVersion: 'Live Version',
      netease: 'Netease'
    },
    sort: {
      fastestRising: 'Fastest Rising',
      mostPopular: 'Most Popular',
      mostRecent: 'Most Recent'
    }
  }
}
