export default {
  projectName: '云音乐在线',
  menus: {
    notLogin: '未登录',
    discovery: '发现音乐',
    recommend: '推荐歌单',
    latestMusic: '最新音乐',
    latestMV: '最新MV',
    recommendSongs: '每日推荐',
    createPlaylist: '我创建的歌单',
    collectPlaylist: '我收藏的歌单',
  },
  message: {
    changeLang: '切换语言成功',
    changeTheme: '切换主题成功',
  },
  latestMusic: {
    all: '全部',
    chinese: '华语',
    western: '欧美',
    japan: '日本',
    korea: '韩国'
  },
  latestMV: {
    area: {
      all: '全部',
      hinterland: '内地',
      hongkongAndTaiwan: '港台',
      europeAndAmerica: '欧美',
      japanese: '日本',
      korea: '韩国',
      other: '其他'
    },
    type: {
      all: '全部',
      official: '官方版',
      original: '原生',
      liveVersion: '现场版',
      netease: '网易出品'
    },
    sort: {
      fastestRising: '上升最快',
      mostPopular: '最热',
      mostRecent: '最新'
    }
  }
}
