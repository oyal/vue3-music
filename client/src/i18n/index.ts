import { createI18n } from 'vue-i18n'
import zhLocale from './lang/zh'
import enLocale from './lang/en'
import { langState } from '@/utils/storage'

const messages = {
  zh: {
    ...zhLocale
  },
  en: {
    ...enLocale
  }
}

const i18n = createI18n({
  legacy: false, // 是一个布尔值，指定是否使用老版本的 Vue-i18n API。默认值为 false
  locale: langState.value, // 是一个字符串，指定当前语言。默认值为 'en'
  messages // 是一个对象，包含各种语言对应的翻译
})

export default i18n
