import { createRouter, createWebHistory, type RouteRecordRaw } from 'vue-router'
import { useNProgress } from '@vueuse/integrations/useNProgress'

const { isLoading } = useNProgress()

// 路由配置
const routes: RouteRecordRaw[] = [
  {
    path: '/',
    name: 'Layout',
    component: () => import('@/layout/index.vue'),
    redirect: '/discovery',
    children: [
      {
        path: 'discovery',
        name: 'Discovery',
        component: () => import('@/views/discovery/index.vue')
      },
      {
        path: 'recommend',
        name: 'Recommend',
        component: () => import('@/views/recommend/index.vue')
      },
      {
        path: 'latestMusic',
        name: 'LatestMusic',
        component: () => import('@/views/latestMusic/index.vue')
      },
      {
        path: 'latestMv',
        name: 'LatestMv',
        component: () => import('@/views/latestMv/index.vue')
      },
      {
        path: 'playlist/:id',
        name: 'Playlist',
        component: () => import('@/views/playlist/index.vue')
      },
      {
        path: 'mv/:id',
        name: 'MV',
        component: () => import('@/views/mv/index.vue')
      },
      {
        path: 'recommend/songs',
        name: 'RecommendSongs',
        component: () => import('@/views/recommendSongs/index.vue')
      },
      {
        path: 'search',
        name: 'Search',
        component: () => import('@/views/search/index.vue')
      }
    ]
  }
]

// 创建路由
const router = createRouter({
  history: createWebHistory(),
  routes
})

// 路由守卫
router.beforeEach((to, from, next) => {
  isLoading.value = true
  next()
})

// 路由守卫
router.afterEach((to, from) => {
  isLoading.value = false
})

export default router
