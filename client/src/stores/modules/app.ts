import { defineStore } from 'pinia'

const useAppStore = defineStore('app', {
  state: () => ({
    isDark: false, // 是否是暗黑模式
  }),
  actions: {
    toggleTheme() { // 切换主题
      this.isDark = !this.isDark
    }
  },
  persist: true // 持久化
})

export default useAppStore
