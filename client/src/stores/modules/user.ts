import { defineStore } from 'pinia'
import request from '@/utils/request'

const useUserStore = defineStore('user', {
  state: () => ({
    cookie: '', // cookie
    userInfo: <any>{}, // 用户信息
    playlist: <any>[] // 用户歌单
  }),
  actions: {
    setCookie(cookie: string) {
      this.cookie = cookie
    },
    async getUserInfo() {
      const data = (await request.get('/login/status', {
        params: {
          timestamp: Date.now()
        }
      })) as any
      this.setUserInfo(data.data.profile)
    },
    setUserInfo(userInfo: any) {
      this.userInfo = userInfo
    },
    async getUserPlaylist() {
      const data = (await request.get('/user/playlist', {
        params: {
          uid: this.userInfo.userId
        }
      })) as any
      this.setPlaylist(data.playlist)
    },
    setPlaylist(playlist: any) {
      this.playlist = playlist
    },
    async logout() {
      this.cookie = ''
      this.userInfo = {}
      this.playlist = []
      await request.get('/logout')
    }
  },
  persist: true
})

export default useUserStore
