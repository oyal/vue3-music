const lazyLoad = {
  mounted(el: any, binding: any) {
    let options = {
      rootMargin: '0px',
      threshold: 0.1
    }

    // 判断元素是否在可视区域内的
    let observer = new IntersectionObserver(entries => {
      entries.forEach(entry => {
        if (entry.isIntersecting) {
          const img = new Image()
          img.src = binding.value
          img.onload = () => {
            el.src = img.src
          }
          observer.unobserve(el)
        }
      })
    }, options)

    observer.observe(el)
  }
}

export default lazyLoad
