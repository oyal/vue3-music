import { createApp } from 'vue'
import App from './App.vue'
import router from '@/router'
import stores from '@/stores'
import i18n from '@/i18n'
import lazyLoad from '@/directives/lazyload'

import '@/styles/index.scss'
import 'virtual:svg-icons-register'

const app = createApp(App)

app.use(router)
app.use(stores)
app.use(i18n)

app.directive('lazyload', lazyLoad)

const meta = document.createElement('meta')
meta.name = 'naive-ui-style'
document.head.appendChild(meta)

app.mount('#app')
