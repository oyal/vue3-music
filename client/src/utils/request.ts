import axios from 'axios'
import useUserStore from '@/stores/modules/user'
import stores from '@/stores'
import { createDiscreteApi } from 'naive-ui'

const { message } = createDiscreteApi(['message'])

const server = axios.create({
  baseURL: import.meta.env.VITE_API_URL, // 基础路径
  timeout: 10000, // 超时时间
  withCredentials: true // 允许携带cookie
})

server.interceptors.request.use(
  request => {
    const userStore = useUserStore(stores)
    if (userStore.cookie) {
      // request.headers.Cookie = userStore.cookie
      // request.
    }
    return request
  },
  error => {
    message.error(error.message)
    return Promise.reject(error)
  }
)

server.interceptors.response.use(
  response => {
    const data = response.data
    if (data.code && data.code !== 200 && ![800, 801, 802, 803].includes(data.code)) {
      message.error(data.message || 'Error')
    }
    return data
  },
  error => {
    message.error(error.message)
    return Promise.reject(error)
  }
)

export default server
