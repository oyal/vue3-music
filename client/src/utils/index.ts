import { h, computed } from 'vue'
import { NIcon } from 'naive-ui'
import SvgIcon from '@/components/SvgIcon/index.vue'
import { useWindowSize } from '@vueuse/core'

export const renderIcon = (name: string) => {
  return () => h(NIcon, null, { default: () => h(SvgIcon, { name }) })
}

const { width } = useWindowSize()
// 判断是否是移动端
export const isMobileTerminal = computed(() => {
  return width.value < 768
})

// 格式化数字
export const formatNumber = (num: number) => {
  if (num >= 100000000) {
    return (num / 100000000).toFixed(1) + '亿'
  } else if (num >= 10000) {
    return (num / 10000).toFixed(1) + '万'
  } else {
    return num.toString()
  }
}

// 格式化时间
// 123211 -> 02:03
export const formatTime = (milliseconds: number) => {
  const seconds = Math.round(milliseconds / 1000)
  const minutes = Math.floor(seconds / 60)
  const remainingSeconds = seconds % 60
  return (minutes < 10 ? '0' : '') + minutes + ':' + (remainingSeconds < 10 ? '0' : '') + remainingSeconds
}

// 格式化日期
// 2021-01-01 12:12:12 -> 2021-01-01

// 1-1 -> 01-01
export const formatDate = (date: Date, fmt: string) => {
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  const o: { [key: string]: number } = {
    'M+': date.getMonth() + 1,
    'd+': date.getDate(),
    'h+': date.getHours(),
    'm+': date.getMinutes(),
    's+': date.getSeconds()
  }
  for (const k in o) {
    if (new RegExp(`(${k})`).test(fmt)) {
      const str = o[k] + ''
      fmt = fmt.replace(RegExp.$1, RegExp.$1.length === 1 ? str : padLeftZero(str))
    }
  }
  return fmt
}

// 补零
function padLeftZero(str: string) {
  return ('00' + str).substr(str.length)
}
