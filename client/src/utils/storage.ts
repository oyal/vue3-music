import { useStorage } from '@vueuse/core'

export const langState = useStorage('lang', 'zh')

export const searchHistoryState = useStorage('searchHistory', <any>[])

export const playlistState = useStorage('playlist', <any>[])

export const currentSongState = useStorage('currentSong', <any>{})

export const volumeState = useStorage('volume', 50)

export const songState = useStorage('playSong', {
  url: '',
  currentTime: 0
})

export const playModeState = useStorage('playMode', <'list' | 'random' | 'single'>'list')
